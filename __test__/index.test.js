const server = require("../src/server");
const supertest = require("supertest");
const requestWithSupertest = supertest(server);

describe("Test server root response", () => {
  it("GET / should return `Renovate GitLab Demo`", async () => {
    const res = await requestWithSupertest.get("/");
    expect(res.status).toEqual(200);
    expect(res.text).toEqual("Renovate GitLab Demo");
  });
});
