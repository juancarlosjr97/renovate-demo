# Renovate Demo

[![pipeline status](https://gitlab.com/juancarlosjr97/renovate-demo/badges/main/pipeline.svg)](https://gitlab.com/juancarlosjr97/renovate-demo/-/commits/main)

This project contains a demo of how Renovate works on GitLab.

```mermaid
graph LR
A --> B
```

## Prerequisites

We need to have installed the following services:

- [GIT](https://git-scm.com/)
- [nvm](https://github.com/nvm-sh/nvm)

and a Unix based computer, such Mac or Ubuntu.

## Demo

The demo is based on a Node application running an express server.

## Installation locally

Install the node version of the project running

```bash
nvm install
```

Install dependencies

```bash
npm ci
```

## Development

To start in development mode run

```bash
npm run start:dev
```

The project will start running in the port `3000` in watch mode using `nodemon`

## Automated tests

To run all automated tests run:

```bash
npm test
```

## Pipeline

The pipeline is running Renovate and only on schedule it will trigger a check to the repository. Each update will create a new PR and will run a pipeline.

## Configure CI/CD variables

### RENOVATE_TOKEN

You need to add a GitLab Personal Access Token (scopes: read_user, api and write_repository) as RENOVATE_TOKEN to CI/CD variables.

### GITHUB_COM_TOKEN

It is also recommended to configure a GitHub.com Personal Access Token (minimum scopes) as GITHUB_COM_TOKEN so that your bot can make authenticated requests to github.com for Changelog retrieval as well as for any dependency that uses GitHub tags.
Without such a token, github.com's API will rate limit requests and make such lookups unreliable.

Documentation available here: [Official GitLab Runner](https://gitlab.com/renovate-bot/renovate-runner) and [Renovate Docs](https://docs.renovatebot.com/gitlab-bot-security/)
