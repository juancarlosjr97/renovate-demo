const express = require("express");

const app = express();

app.get("/", (req, res) => {
  res.send("Renovate GitLab Demo");
});

module.exports = app;
