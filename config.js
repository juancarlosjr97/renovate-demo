module.exports = {
  onboardingConfig: {
    extends: ["config:base"],
  },
  platform: "gitlab",
  gitAuthor: "Renovate Bot <juancarlosjr97@gmail.com>",
  baseBranches: ["main"],
  labels: ["dependencies"],
  packageRules: [],
  repositories: ["juancarlosjr97/renovate-demo"],
};
